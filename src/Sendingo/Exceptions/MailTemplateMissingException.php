<?php

namespace Sendingo\Exceptions;


/**
 * MailTemplateMissingException is thrown when Sendingo can't find requested template
 *
 * @package Sendingo
 */
final class MailTemplateMissingException extends \Exception
{

}