<?php

namespace Sendingo\Exceptions;


/**
 * InternalServerException is thrown when server returns 500-599 error code.
 *
 * @package Sendingo
 */
final class InternalServerException extends \Exception
{

}