<?php

namespace Sendingo\Exceptions;


/**
 * ConnectionException is thrown when can't connect with sendingo server.
 *
 * @package Sendingo
 */
final class ConnectionException extends \Exception
{

}