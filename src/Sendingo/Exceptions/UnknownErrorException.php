<?php

namespace Sendingo\Exceptions;


/**
 * UnknownErrorException is thrown when we can't identify source of problem.
 *
 * @package Sendingo
 */
final class UnknownErrorException extends \Exception
{

}