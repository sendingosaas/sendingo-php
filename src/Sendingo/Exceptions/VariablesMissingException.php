<?php

namespace Sendingo\Exceptions;


/**
 * VariablesMissingException is thrown when not all required variables were passed in request body
 * while sending data to template.
 *
 * @package Sendingo
 */
final class VariablesMissingException extends \Exception
{

}