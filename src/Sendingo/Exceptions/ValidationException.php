<?php

namespace Sendingo\Exceptions;


/**
 * ValidationException is thrown when credentials from .env file are incorrect.
 *
 * @package Sendingo
 */
final class ValidationException extends \Exception
{

}