<?php

namespace Sendingo\Api;

/**
 * Class Mail
 *
 * @package Sendingo\Api
 */
class Mail extends HttpApi
{
    /**
     * Send e-mail
     *
     * @param string $email
     * @param string $template_slug
     * @param array $variables
     * @param string $lang
     * @return mixed
     */
    public function send($email, $template_slug, array $variables = [], $lang = null)
    {
        $params = [
            'email' => $email,
            'template' => $template_slug,
            'variables' => $variables,
            'lang'  => $lang,
        ];

        return $this->httpPost('/mail/send', $params);
    }
}