<?php

namespace Sendingo\Api;


use Sendingo\Exceptions\ConnectionException;
use Sendingo\Exceptions\EmailFormatException;
use Sendingo\Exceptions\InternalServerException;
use Sendingo\Exceptions\UnknownErrorException;
use Sendingo\Exceptions\ValidationException;
use Sendingo\Exceptions\VariablesMissingException;

/**
 * Class HttpApi
 *
 * @package Sendingo
 */
class HttpApi
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var array
     */
    private $credentials;

    /**
     * HttpApi constructor.
     *
     * @param string $endpoint
     * @param array $credentials
     */
    public function __construct($endpoint, array $credentials)
    {
        $this->endpoint = $endpoint;
        $this->credentials = $credentials;
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array
     */
    public function httpGet($path, array $parameters = [])
    {
        return $this->decodeResponse($this->httpRawCall('GET', $path, $parameters));
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array
     */
    public function httpPost($path, array $parameters = [])
    {
        return $this->decodeResponse($this->httpRawCall('POST', $path, $parameters));
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array
     */
    public function httpPut($path, array $parameters = [])
    {
        return $this->decodeResponse($this->httpRawCall('PUT', $path, $parameters));
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array
     */
    public function httpPatch($path, array $parameters = [])
    {
        return $this->decodeResponse($this->httpRawCall('PATCH', $path, $parameters));
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array
     */
    public function httpDelete($path, array $parameters = [])
    {
        return $this->decodeResponse($this->httpRawCall('DELETE', $path, $parameters));
    }

    /**
     * @param string $method (GET/POST/PUT/PATCH/DELETE)
     * @param string $path
     * @param array $parameters Request body data
     * @return string JSON parsed response
     * @throws ConnectionException
     */
    private function httpRawCall($method, $path, array $parameters = [])
    {
        $this->attachSignature($parameters);

        try
        {
            $url = $this->endpoint.$path;

            if($method == 'GET') {
                $url .= '?'.http_build_query($parameters);
            }
            $data_string = json_encode($parameters);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string),
                    'User-Agent', 'sendingo-sdk-php/v1'
            ));

            $raw_response = curl_exec($ch);

            $response = [
                'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
                'body'      => $raw_response,
            ];

            curl_close($ch);
        }
        catch (\Exception $e)
        {
            throw new ConnectionException($e->getMessage());
        }

        return $this->handleErrors($response);
    }

    /**
     * Attach singature to given array with request body data.
     *
     * @param array $parameters
     */
    private function attachSignature(array &$parameters)
    {
        $parameters['client_id']    = $this->credentials['client_id'];
        $parameters['site_id']      = $this->credentials['site_id'];
        $parameters['hash']         = $this->calculateHash($parameters);
    }

    /**
     * Calculates hash from given assoc array.
     *
     * @param array $parameters
     * @return string SHA-1 hash
     */
    private function calculateHash(array $parameters)
    {
        // TODO: Hashing function have to be identical as in API
        $string = '';
        foreach ($parameters as $parameter) {
            if(is_array($parameter)) {
                foreach ($parameter as $subParameter) {
                    if(is_array($subParameter)) $subParameter = json_encode($subParameter);
                    $string .= $subParameter;
                }
            } else {
                $string .= $parameter;
            }
        }

        // Warning: SHA-1 was cracked by Google in 2017 and it is considered as unsafe.
        return sha1($string);
    }

    /**
     * @param string $response
     * @return array
     */
    private function decodeResponse($response)
    {
        return json_decode($response, true);
    }

    /**
     * Process http response and throw error according to status code.
     *
     * @param array $response
     * @return string Response body
     * @throws EmailFormatException
     * @throws InternalServerException
     * @throws UnknownErrorException
     * @throws ValidationException
     * @throws VariablesMissingException
     */
    private function handleErrors($response)
    {
        $statusCode = $response['http_code'];
        switch ($statusCode) {
            case 200:
                return $response['body'];
            case 422:
                throw new EmailFormatException();
            case 401:
                throw new ValidationException();
            case 402:
                throw new VariablesMissingException();
            case 500 <= $statusCode:
                throw new InternalServerException('Internal server error', $statusCode);
            default:
                throw new UnknownErrorException($statusCode);
        }
    }
}