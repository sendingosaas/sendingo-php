<?php

namespace Sendingo\Api;

/**
 * Class User
 *
 * @package Sendingo\Api
 */
class User extends HttpApi
{
    /**
     * Subscribe user.
     *
     * @param string $email
     * @param string $lang
     * @return array
     */
    public function subscribe($email, $lang = '')
    {
        $params = [
            'email' => $email,
            'lang'  => $lang,
        ];

        return $this->httpPost('/user/subscribe', $params);
    }

    /**
     * Unsubscribe user.
     *
     * @param string $email
     * @return array
     */
    public function unsubscribe($email)
    {
        $params = ['email' => $email];

        return $this->httpDelete('/user/unsubscribe', $params);
    }

    /**
     * Change user's e-mail.
     *
     * @param string $email
     * @param string $new_email
     * @return array
     */
    public function changeEmail($email, $new_email)
    {
        $params = [
            'email'     => $email,
            'new_email' => $new_email,
        ];

        return $this->httpPatch('/user/email', $params);
    }

    /**
     * Change user's custom attributes.
     *
     * @param string $email
     * @param string $data
     * @return array
     */
    public function updateData($email, $data)
    {
        $params = [
            'email'     => $email,
            'data' => $data,
        ];

        return $this->httpPatch('/user/data', $params);
    }
}