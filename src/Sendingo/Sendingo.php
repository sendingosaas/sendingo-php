<?php

/*
 * Copyright (C) 2013-2016 Sendingo
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace Sendingo;
use Sendingo\Exceptions\ValidationException;


/**
 * Class Sendingo
 *
 * @package Sendingo
 */
class Sendingo
{
    /**
     * URL to communicate with Sendingo API
     *
     * @var string
     */
    private $endpoint = 'https://api.sendingo.com';

    /**
     * Array with key, client_id and site_id
     *
     * @var array
     */
    private $credentials;

    /**
     * Sendingo constructor.
     *
     * @param $key
     * @param null $client_id
     * @param null $site_id
     */
    public function __construct($key, $client_id = null, $site_id = null)
    {
        $this->credentials = [
            'client_id' => $client_id,
            'site_id'   => $site_id,
            'key'       => $key,
        ];

        $this->validateCredentials();
    }

    /**
     * @return Api\User
     */
    public function users()
    {
        return new Api\User($this->endpoint, $this->credentials);
    }

    /**
     * @return Api\Mail
     */
    public function mails()
    {
        return new Api\Mail($this->endpoint, $this->credentials);
    }

    /**
     * If client_id is not null, site_id must be null, and vice versa. Both can't be null.
     *
     * @throws ValidationException
     */
    private function validateCredentials()
    {
        if( ($this->credentials['site_id'] === $this->credentials['client_id'])
            || ( ! is_null($this->credentials['site_id']) && ! is_null($this->credentials['client_id'])) ) {
            throw new ValidationException();
        }
    }
}