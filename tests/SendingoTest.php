<?php

namespace Sendingo\Tests;


use PHPUnit\Framework\TestCase;
use Sendingo\Api\Mail;
use Sendingo\Api\User;
use Sendingo\Exceptions\ConnectionException;
use Sendingo\Exceptions\UnknownErrorException;
use Sendingo\Sendingo;

class SendingoTest extends TestCase
{

    public function testSendingo()
    {
        $sendingo = new Sendingo('abc', 1);

        $this->assertInstanceOf(Sendingo::class, $sendingo);
    }

    public function testUser()
    {
        $sendingo = new Sendingo('abc', 1);

        $this->assertInstanceOf(User::class, $sendingo->users());
    }

    public function testMail()
    {
        $sendingo = new Sendingo('abc', 1);

        $this->assertInstanceOf(Mail::class, $sendingo->mails());
    }

    public function testSendMail()
    {
        $this->expectException(UnknownErrorException::class);

        $sendingo = new Sendingo('abc', 1);

        $sendingo->mails()->send('example@example.com', 'promo-template-slug', [
            'company_name' => 'Sendingo Sp. z o.o.',
            'message'      => 'We\'ve reached 1000000 customers!',
            'promo_code'   => 'DISCOUNT',
        ], 'pl_PL');
    }
}