# Sendingo PHP client

This is the Sendingo PHP SDK, which contains methods for easly 
interacting with the Sendingo API.

## Installation

To install the SDK, you will need to be using [Composer](http://getcomposer.org/) 
in your project. 
If you aren't using Composer yet, it's really simple! Here's how to install 
composer:

```bash
curl -sS https://getcomposer.org/installer | php
```

To get started just run the following command (package is under development):
```bash
php composer.phar require sendingosaas/sendingo-php:dev-master
```
Later, on production:
```bash
php composer.phar require sendingosaas/sendingo-php:*
```

## Usage
You should always use Composer's autoloader in your application to automatically 
load the your dependencies.
All examples below assumes you've already included this in your file:

```php
require 'vendor/autoload.php';
use Sendingo\Sendingo;
```

Here's how to send a message using the SDK:
```php
# First, instantiate the SDK with your API credentials.
$client_id = 1;
$sendingo = new Sendingo('api-key-example', $client_id);

# Now, compose and send your message.
$sendingo->mails()->send('example@example.com', 'promo-template-slug', [
  'company_name' => 'Sendingo Sp. z o.o.', 
  'message'      => 'We\'ve reached 1000000 customers!', 
  'promo_code'   => 'DISCOUNT', 
], 'pl_PL');
```